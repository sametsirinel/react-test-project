import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Topbar from "./components/topbar/topbar";
import Specialnavbar from "./components/navbar/specialnavbar";
import Home from "./components/Pages/home";
import Other from "./components/Pages/other";
import './App.css';


class App extends Component {
  render() {
    return (
        <Router>
          <div>
            <Topbar />
            <Specialnavbar />
            <Route exact path="/" component={Home} />
            <Route exact path="/other" component={Other} />
          </div>
        </Router>
    );
  }
}

export default App;
