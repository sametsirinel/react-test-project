import React, { Component } from 'react';
import "./items.css";
import Item from './item';

class Items extends Component {

    state = {
        data: []
    }
    constructor(props){
        super(props);

        this.fetchData();
    }
    fetchData() {

        fetch('https://jsonplaceholder.typicode.com/photos?_limit=3')
          .then(response => response.json())
          .then(data => this.setState({ data: data }));
          
    }

    render() {

        const data = this.state.data;

        return (
            <div>
                <div className="centered">
                    <div className="contents">
                        {   
                            data.length>0 ?
                                
                                data.map(content => {

                                    return (
                                        <Item key={content.id} Image={content.thumbnailUrl} Title={content.title} Description={content.title}/>
                                    )
                                })

                            : null

                        }                        
                    </div>
                </div>
            </div>
        );
    }
}

export default Items;
