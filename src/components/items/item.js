import React, { Component } from 'react';
import "./item.css";

class Item extends Component {
    
    render(){
        const {Image,Title,Description} = this.props; 
        return (
            <div>
                <div className="item">
                    <div className="photo">
                        <img src={Image} alt={Title}/>
                    </div>
                    <div className="text">
                        <h2>{Title.split(' ')[0]}</h2>
                        <p>{Description}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Item;
