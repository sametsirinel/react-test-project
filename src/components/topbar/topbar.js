import React, { Component } from 'react';
import "./topbar.css";
import Logo from '../../../public/Logo.png';

class topbar extends Component {
  render() {
    return (
      <div>
          <div className="top-bar">
            <div className="centered">
                <div className="logo">
                    <img src={Logo} alt="aaa"/>
                </div>
                <div className="lang">
                    <a href="">EN</a>
                    <a href="#">|</a>
                    <a href="">TR</a>
                </div>
            </div>
          </div>
      </div>
    );
  }
}

export default topbar;
