import React, { Component } from 'react';
import Banner from "../../components/banner/banner";
import BannerImg from '../../../public/big-photo.png';
import Items from "../../components/items/items";

class home extends Component {
  render() {
    return (
      <div>
        <Banner BannerImg={BannerImg}/>
        <Items />
      </div>
    );
  }
}

export default home;
