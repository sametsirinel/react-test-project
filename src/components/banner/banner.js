import React, { Component } from 'react';
import "./banner.css";

class Banner extends Component {
  render() {
    const {BannerImg} = this.props;
    return (
      <div>
        <div className="banner">
            <img src={BannerImg} alt="aaa" />
        </div>
      </div>
    );
  }
}

export default Banner;
