import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "./specialnavbar.css";

class SpecialNavbar extends Component {

  state = {
    isVisiable:false,
  }

  render() {
    
    return (
      <div>
        <nav>
          <button className="menuButton">Menü</button>
          <ul className="navbar">
            <li><Link to="/">Platform</Link></li>
            <li>
              <a href="#">Products</a>
              <div className="submenu">
                <ul>
                  <li><a href="" className="active">On Boarding</a></li>
                  <li><a href="">Customisations</a></li>
                  <li><a href="">Templates</a></li>
                  <li><a href="">Integrations</a></li>
                </ul>
              </div>
            </li>
            <li><Link to="/other">Solitions</Link></li>
            <li><a href="">Services</a></li>
            <li><a href="">Pricing</a></li>
          </ul>
      </nav>
      </div>
    );
  }
}

export default SpecialNavbar;
